package in.net.webinfotech.fluffy.threading;

import android.os.Handler;
import android.os.Looper;

import in.net.webinfotech.fluffy.domain.executor.MainThread;

/**
 * Created by Raj on 20-05-2019.
 */

public class MainThreadImpl implements MainThread {
    private static MainThread sMainThread;

    private Handler mHandler;

    private MainThreadImpl() {
        mHandler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void post(Runnable runnable) {
        mHandler.post(runnable);
    }

    public static MainThread getInstance() {
        if (sMainThread == null) {
            sMainThread = new MainThreadImpl();
        }

        return sMainThread;
    }
}
