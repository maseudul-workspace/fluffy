package in.net.webinfotech.fluffy.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.fluffy.R;
import in.net.webinfotech.fluffy.domain.model.Testing.Ratings;
import in.net.webinfotech.fluffy.util.GlideHelper;

/**
 * Created by Raj on 31-05-2019.
 */

public class RatingsAdapter extends RecyclerView.Adapter<RatingsAdapter.ViewHolder>{

    Context mContext;
    ArrayList<Ratings> ratings;

    public RatingsAdapter(Context mContext, ArrayList<Ratings> ratings) {
        this.mContext = mContext;
        this.ratings = ratings;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_ratings, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewUserName.setText(ratings.get(position).user);
        holder.txtViewDate.setText(ratings.get(position).date);
        holder.ratingBar.setRating(ratings.get(position).rating);
        String nameInitial = getInitial(ratings.get(position).user);
        switch (nameInitial){
            case "A":
                GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewUser, R.drawable.a);
                break;
            case "M":
                GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewUser, R.drawable.m);
                break;
            case "P":
                GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewUser, R.drawable.p);
                break;
            case "R":
                GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewUser, R.drawable.r);
                break;
            case "S":
                GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewUser, R.drawable.s);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return ratings.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_user_name)
        TextView txtViewUserName;
        @BindView(R.id.img_view_user_name)
        ImageView imgViewUser;
        @BindView(R.id.rating_bar_user)
        RatingBar ratingBar;
        @BindView(R.id.txt_view_rating_date)
        TextView txtViewDate;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public String getInitial(String s){
        String[] stringArr = s.split("");
        return stringArr[1];
    }

}
