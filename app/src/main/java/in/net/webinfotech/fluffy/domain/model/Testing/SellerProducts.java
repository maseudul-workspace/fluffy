package in.net.webinfotech.fluffy.domain.model.Testing;

/**
 * Created by Raj on 31-05-2019.
 */

public class SellerProducts {
    public String products;
    public double price;

    public SellerProducts(String products, double price) {
        this.products = products;
        this.price = price;
    }
}
