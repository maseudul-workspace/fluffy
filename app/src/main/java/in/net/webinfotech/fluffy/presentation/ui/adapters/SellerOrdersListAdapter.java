package in.net.webinfotech.fluffy.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.fluffy.R;
import in.net.webinfotech.fluffy.domain.model.Testing.SellerOrders;

/**
 * Created by Raj on 19-06-2019.
 */

public class SellerOrdersListAdapter extends RecyclerView.Adapter<SellerOrdersListAdapter.ViewHolder> {

    public interface Callback{
        void onAcceptClicked(int position);
        void onRejectClicked();
    }

    ArrayList<SellerOrders> sellerOrders;
    Context mContext;
    Callback mCallback;

    public SellerOrdersListAdapter(ArrayList<SellerOrders> sellerOrders, Context mContext, Callback callback) {

        this.sellerOrders = sellerOrders;
        this.mContext = mContext;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_seller_orders_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.txtViewProductName.setText(sellerOrders.get(position).productName);
        holder.txtViewQuantity.setText(Integer.toString(sellerOrders.get(position).quantity));
        holder.txtViewBuyerName.setText(sellerOrders.get(position).orderedBy);
        holder.txtViewBuyerPhoneNo.setText(sellerOrders.get(position).phoneNumber);
        holder.txtViewDeliveryAddress.setText(sellerOrders.get(position).address);
        holder.btnAcceptOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onAcceptClicked(position);
            }
        });
        holder.btnRejectOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onRejectClicked();
            }
        });
    }

    @Override
    public int getItemCount() {
        return sellerOrders.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.txt_view_qty)
        TextView txtViewQuantity;
        @BindView(R.id.txt_view_buyer_name)
        TextView txtViewBuyerName;
        @BindView(R.id.txt_view_buyer_phone_no)
        TextView txtViewBuyerPhoneNo;
        @BindView(R.id.txt_view_delivery_address)
        TextView txtViewDeliveryAddress;
        @BindView(R.id.btn_accept_order)
        Button btnAcceptOrder;
        @BindView(R.id.btn_reject_order)
        Button btnRejectOrder;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
