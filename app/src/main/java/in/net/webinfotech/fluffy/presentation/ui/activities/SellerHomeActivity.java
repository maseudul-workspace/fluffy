package in.net.webinfotech.fluffy.presentation.ui.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.common.SignInButton;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.fluffy.R;
import in.net.webinfotech.fluffy.domain.model.Testing.SellerProductList;
import in.net.webinfotech.fluffy.presentation.ui.adapters.SellerProductListAdapter;
import in.net.webinfotech.fluffy.presentation.ui.bottomsheets.SellerProductUploadFormBottomSheet;
import in.net.webinfotech.fluffy.presentation.ui.dialogs.UpdateProductDialog;

public class SellerHomeActivity extends AppCompatActivity implements SellerProductUploadFormBottomSheet.Callback, SellerProductListAdapter.Callback{

    @BindView(R.id.recycler_view_seller_products)
    RecyclerView recyclerViewSellerProducts;
    SellerProductUploadFormBottomSheet uploadFormBottomSheet;
    UpdateProductDialog updateProductDialog;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_home);
        ButterKnife.bind(this);
        setBottomNavigationMenu();
        setRecyclerViewSellerProducts();
        initialiseProductUploadBottomSheet();
        initialiseDialog();
    }

    public void initialiseProductUploadBottomSheet(){
        uploadFormBottomSheet = new SellerProductUploadFormBottomSheet();
    }

    public void setBottomNavigationMenu(){
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_orders:
                        Intent intent = new Intent(getApplicationContext(), SellerOrdersActivity.class);
                        startActivity(intent);
                        break;
                }
                return false;
            }
        });
    }


    public void initialiseDialog(){
        updateProductDialog = new UpdateProductDialog(this, this);
    }

    public void setRecyclerViewSellerProducts(){
        ArrayList<SellerProductList> sellerProductLists = new ArrayList<>();

        SellerProductList sellerProductList1 = new SellerProductList("Live Chicken[1.1 - 1.3]", 100, 100, true);
        SellerProductList sellerProductList2  = new SellerProductList("Live Chicken[1.3 - 1.6]", 120, 29, false);
        SellerProductList sellerProductList3 = new SellerProductList("Live Chicken[1.6 - 1.8]", 150, 56, true);

        sellerProductLists.add(sellerProductList1);
        sellerProductLists.add(sellerProductList2);
        sellerProductLists.add(sellerProductList3);

        SellerProductListAdapter adapter = new SellerProductListAdapter(sellerProductLists, this);
        recyclerViewSellerProducts.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewSellerProducts.setAdapter(adapter);

    }

    @Override
    public void onAddClicked() {
        uploadFormBottomSheet.dismiss();
    }

    @OnClick(R.id.layout_product_add) void onProductAddClicked(){
        uploadFormBottomSheet.show(getSupportFragmentManager(), "Choose Product");
    }

    @Override
    public void onEditClicked() {
        updateProductDialog.showDialog(1, "");
    }
}
