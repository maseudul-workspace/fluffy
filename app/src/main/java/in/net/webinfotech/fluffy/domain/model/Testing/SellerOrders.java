package in.net.webinfotech.fluffy.domain.model.Testing;

/**
 * Created by Raj on 19-06-2019.
 */

public class SellerOrders {
    public String productName;
    public int quantity;
    public String orderedBy;
    public String phoneNumber;
    public String address;

    public SellerOrders(String productName, int quantity, String orderedBy, String phoneNumber, String address) {
        this.productName = productName;
        this.quantity = quantity;
        this.orderedBy = orderedBy;
        this.phoneNumber = phoneNumber;
        this.address = address;
    }
}
