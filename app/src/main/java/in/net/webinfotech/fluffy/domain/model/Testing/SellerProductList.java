package in.net.webinfotech.fluffy.domain.model.Testing;

/**
 * Created by Raj on 03-06-2019.
 */

public class SellerProductList {

    public String productName;
    public double price;
    public int quantity;
    public boolean isAvailable;

    public SellerProductList(String productName, double price, int quantity, boolean isAvailable) {
        this.productName = productName;
        this.price = price;
        this.quantity = quantity;
        this.isAvailable = isAvailable;
    }

}
