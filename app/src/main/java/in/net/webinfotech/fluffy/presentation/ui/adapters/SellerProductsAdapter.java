package in.net.webinfotech.fluffy.presentation.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.fluffy.R;
import in.net.webinfotech.fluffy.domain.model.Testing.SellerProducts;

/**
 * Created by Raj on 31-05-2019.
 */

public class SellerProductsAdapter extends RecyclerView.Adapter<SellerProductsAdapter.ViewHolder>{


    ArrayList<SellerProducts> sellerProducts;
    public SellerProductsAdapter(ArrayList<SellerProducts> sellerProducts) {
        this.sellerProducts = sellerProducts;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_seller_products, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewProduct.setText(sellerProducts.get(position).products);

    }

    @Override
    public int getItemCount() {
        return sellerProducts.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_product)
        TextView txtViewProduct;
        @BindView(R.id.img_view_add_to_cart)
        ImageView imgViewAddToCart;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
