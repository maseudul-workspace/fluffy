package in.net.webinfotech.fluffy.presentation.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;


import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.fluffy.R;
import in.net.webinfotech.fluffy.presentation.ui.bottomsheets.SellerTypeBottomSheet;

public class CreateProfileActivity extends AppCompatActivity implements SellerTypeBottomSheet.Callback {

    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.edit_text_pin)
    EditText editTextPin;
    @BindView(R.id.edit_text_locality)
    EditText editTextLocality;
    @BindView(R.id.edit_text_landmark)
    EditText editTextLandmark;
    @BindView(R.id.edit_text_street)
    EditText editTextStreet;
    @BindView(R.id.edit_text_business_name)
    EditText editTextBusinessName;
    @BindView(R.id.edit_text_gst)
    EditText editTextGST;
    @BindView(R.id.edit_text_delivery)
    EditText editTextDelivery;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_referal_code)
    EditText editTextReferalCode;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    @BindView(R.id.edit_text_repeat_password)
    EditText editTextRepeatPassword;
    @BindView(R.id.relative_layout_personal_details)
    View relativeLayoutPersonal;
    @BindView(R.id.relative_layout_business)
    View relativeLayoutBusiness;
    @BindView(R.id.relative_layout_account)
    View relativeLayoutAccount;
    @BindView(R.id.view_round_personal)
    View viewRoundPersonal;
    @BindView(R.id.view_round_business)
    View viewRoundBusiness;
    @BindView(R.id.view_round_account)
    View viewRoundAccount;
    @BindView(R.id.view_line_business)
    View viewLineBusiness;
    @BindView(R.id.view_line_account)
    View viewLineAccount;
    @BindView(R.id.radio_group_business_type)
    RadioGroup radioGroupBusinessType;
    @BindView(R.id.radio_btn_seller)
    RadioButton radioBtnSeller;
    @BindView(R.id.radio_btn_buyer)
    RadioButton radioBtnBuyer;
    int PLACE_PICKER_REQUEST = 1;
    String businessType = "";
    String sellerType = "Shopkeeper";
    SellerTypeBottomSheet sellerTypeBottomSheet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_profile);
        ButterKnife.bind(this);
        setUpRadioBtnListeners();
    }

    public void setUpRadioBtnListeners(){
        radioGroupBusinessType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton radioButton = (RadioButton)radioGroup.findViewById(i);
                businessType = radioButton.getText().toString();
                Log.e("LogMsg", businessType);
            }
        });
        radioBtnBuyer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSellerTypeBottomSheet();
            }
        });
    }

    public void showSellerTypeBottomSheet(){
        Bundle args = new Bundle();
        args.putString("sellerType", sellerType);
        sellerTypeBottomSheet = new SellerTypeBottomSheet();
        sellerTypeBottomSheet.setArguments(args);
        sellerTypeBottomSheet.show(getSupportFragmentManager(), "Select seller type");
    }

    @OnClick(R.id.btn_next_personal)void onPersonalNextClicked(){
        viewRoundBusiness.setBackground(getDrawable(R.drawable.rounded_app_color_rectangle));
        viewLineBusiness.setBackground(getDrawable(R.drawable.rounded_app_color_rectangle));
        relativeLayoutPersonal.setVisibility(View.GONE);
        relativeLayoutBusiness.setVisibility(View.VISIBLE);
        relativeLayoutAccount.setVisibility(View.GONE);
    }

    @OnClick(R.id.btn_next_business)void onBusinessNextClicked(){
        viewLineAccount.setBackground(getDrawable(R.drawable.rounded_app_color_rectangle));
        viewRoundAccount.setBackground(getDrawable(R.drawable.rounded_app_color_rectangle));
        relativeLayoutPersonal.setVisibility(View.GONE);
        relativeLayoutBusiness.setVisibility(View.GONE);
        relativeLayoutAccount.setVisibility(View.VISIBLE);

    }

    @OnClick(R.id.btn_prev_business) void onBusinessPrevClicked(){
        viewRoundBusiness.setBackground(getDrawable(R.drawable.rounded_grey_rectangle));
        viewLineBusiness.setBackground(getDrawable(R.drawable.rounded_grey_rectangle));
        relativeLayoutPersonal.setVisibility(View.VISIBLE);
        relativeLayoutBusiness.setVisibility(View.GONE);
        relativeLayoutAccount.setVisibility(View.GONE);
    }

    @OnClick(R.id.btn_prev_account) void onAccountPrevClicked(){
        viewLineAccount.setBackground(getDrawable(R.drawable.rounded_grey_rectangle));
        viewRoundAccount.setBackground(getDrawable(R.drawable.rounded_grey_rectangle));
        relativeLayoutPersonal.setVisibility(View.GONE);
        relativeLayoutBusiness.setVisibility(View.VISIBLE);
        relativeLayoutAccount.setVisibility(View.GONE);
    }


    @Override
    public void onRadioBtnSelected(String msg) {
        sellerType = msg;
    }

    @OnClick(R.id.btn_next_account) void btnNextAccountClicked(){
        Log.e("LogMsg", businessType);
        if(businessType.equals("Buyer")){
            Intent buyerHomeIntent = new Intent(this, BuyerHomeActivity.class);
            startActivity(buyerHomeIntent);
        }else{
            Intent sellerHomeIntent = new Intent(this, SellerHomeActivity.class);
            startActivity(sellerHomeIntent);
        }

    }

}
