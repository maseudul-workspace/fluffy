package in.net.webinfotech.fluffy.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.View;

import in.net.webinfotech.fluffy.R;

/**
 * Created by Raj on 15-06-2019.
 */

public class UpdateProductDialog {

    AlertDialog.Builder builder;
    Context mContext;
    Activity mActivity;
    AlertDialog dialog;
    View dialogContainer;

    public UpdateProductDialog(Context mContext, Activity mActivity) {
        this.mContext = mContext;
        this.mActivity = mActivity;
    }

    public void showDialog(int quanitity, String productName){
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.dialog_product_update, null);

        builder = new AlertDialog.Builder(mContext);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.show();

    }

}
