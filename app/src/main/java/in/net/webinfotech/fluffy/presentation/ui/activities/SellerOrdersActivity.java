package in.net.webinfotech.fluffy.presentation.ui.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.fluffy.R;
import in.net.webinfotech.fluffy.domain.model.Testing.SellerOrders;
import in.net.webinfotech.fluffy.presentation.ui.adapters.SellerOrdersListAdapter;
import in.net.webinfotech.fluffy.presentation.ui.dialogs.OrderRejectDialog;

public class SellerOrdersActivity extends AppCompatActivity implements SellerOrdersListAdapter.Callback{

    @BindView(R.id.recycler_view_seller_orders)
    RecyclerView recyclerViewSellerOrders;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;
    ArrayList<SellerOrders> sellerOrders;
    OrderRejectDialog orderRejectDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_orders);
        ButterKnife.bind(this);
        setBottomNavigationMenu();
        setRecyclerViewSellerOrders();
        initialiseRejectDialog();
    }

    public void initialiseRejectDialog(){
        orderRejectDialog = new OrderRejectDialog(this, this);
    }

    public void setBottomNavigationMenu(){
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(2);
        menuItem.setChecked(true);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_home:
                        Intent intent = new Intent(getApplicationContext(), SellerHomeActivity.class);
                        startActivity(intent);
                        break;
                }
                return false;
            }
        });
    }

    public void setRecyclerViewSellerOrders(){
        sellerOrders = new ArrayList<>();

        SellerOrders sellerOrders1 = new SellerOrders("Live Chicken[1.1 - 1.3]", 10, "Peter Parker", "7663109366", "Near Govindam Restaurant, Downtown, Guwahati, Assam, 781335");
        SellerOrders sellerOrders2 = new SellerOrders("Live Chicken[1.3 - 1.6]", 10, "Johnny Blaze", "423311366", "Near PragJyotish Anundoram School, Maligaon, Guwahati, Assam, 781335");
        SellerOrders sellerOrders3 = new SellerOrders("Live Chicken[1.6 - 1.8]", 10, "Tony Stark", "434311366", "Near Grand Starlight Hotel, Silpukhiri, Guwahati, Assam, 781338");

        sellerOrders.add(sellerOrders1);
        sellerOrders.add(sellerOrders2);
        sellerOrders.add(sellerOrders3);

        SellerOrdersListAdapter adapter = new SellerOrdersListAdapter(sellerOrders, this, this);

        recyclerViewSellerOrders.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewSellerOrders.setAdapter(adapter);

    }

    @Override
    public void onAcceptClicked(int position) {
        showConfirmationDialog(sellerOrders.get(position));
    }

    @Override
    public void onRejectClicked() {
        orderRejectDialog.showDialog();
    }

    public void showConfirmationDialog(SellerOrders sellerOrders){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Order Confirmation");
        builder.setMessage("You are about to deliver " + sellerOrders.productName + " to "+ sellerOrders.orderedBy + " at "+ sellerOrders.address + " on 12 july, 2019 7:09 am. Do you really want to proceed ?");
        builder.setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.show();
    }
}
