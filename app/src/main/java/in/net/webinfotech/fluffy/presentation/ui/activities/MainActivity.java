package in.net.webinfotech.fluffy.presentation.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.fluffy.R;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.edit_text_phone_number)
    EditText editTextPhoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_get_otp) void onGetOTPClicked(){
        Intent getOTPIntent = new Intent(this, PhoneNumberVerifyActivity.class);
        getOTPIntent.putExtra("phoneNumber", editTextPhoneNumber.getText().toString());
        startActivity(getOTPIntent);
    }

}
