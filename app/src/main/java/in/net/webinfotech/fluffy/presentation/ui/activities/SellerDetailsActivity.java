package in.net.webinfotech.fluffy.presentation.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.fluffy.R;
import in.net.webinfotech.fluffy.domain.model.Testing.Ratings;
import in.net.webinfotech.fluffy.domain.model.Testing.SellerDeliveryLocation;
import in.net.webinfotech.fluffy.domain.model.Testing.SellerProducts;
import in.net.webinfotech.fluffy.presentation.ui.adapters.RatingsAdapter;
import in.net.webinfotech.fluffy.presentation.ui.adapters.SellerDeliveryLocationAdapter;
import in.net.webinfotech.fluffy.presentation.ui.adapters.SellerProductsAdapter;

public class SellerDetailsActivity extends AppCompatActivity {

    @BindView(R.id.recycler_view_ratings)
    RecyclerView recyclerViewRatings;
    @BindView(R.id.recycler_view_seller_location)
    RecyclerView recyclerViewSellerLocation;
    @BindView(R.id.recycler_view_slabs)
    RecyclerView recyclerViewSlabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_details);
        ButterKnife.bind(this);
        setRecyclerViewRatings();
        setRecyclerViewSellerLocation();
        setRecyclerViewSlabs();
    }

    public void setRecyclerViewSellerLocation(){
        ArrayList<SellerDeliveryLocation> locations = new ArrayList<>();

        SellerDeliveryLocation location1 = new SellerDeliveryLocation("Jalukbari, Guwahati");
        SellerDeliveryLocation location2 = new SellerDeliveryLocation("Paltanbajar, Guwahati");
        SellerDeliveryLocation location3 = new SellerDeliveryLocation("Sixmile Guwahati");
        SellerDeliveryLocation location4 = new SellerDeliveryLocation("Maligaon, Near A.T road, Guwahati");

        locations.add(location1);
        locations.add(location2);
        locations.add(location3);
        locations.add(location4);

        SellerDeliveryLocationAdapter adapter = new SellerDeliveryLocationAdapter(locations);

        recyclerViewSellerLocation.setAdapter(adapter);
        recyclerViewSellerLocation.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewSellerLocation.setNestedScrollingEnabled(false);
    }

    public void setRecyclerViewSlabs(){
        ArrayList<SellerProducts> sellerProducts = new ArrayList<>();

        SellerProducts sellerProducts1 = new SellerProducts("Live Chicken[1.1 - 1.3]", 100);
        SellerProducts sellerProducts2 = new SellerProducts("Live Chicken[1.3 - 1.6]", 110);
        SellerProducts sellerProducts3 = new SellerProducts("Live Chicken[1.6 - 1.8]", 150);
        SellerProducts sellerProducts4 = new SellerProducts("Live Chicken[1.8 - 2.2]", 200);
        SellerProducts sellerProducts5 = new SellerProducts("Live Chicken[22 and above]", 220);

        sellerProducts.add(sellerProducts1);
        sellerProducts.add(sellerProducts2);
        sellerProducts.add(sellerProducts3);
        sellerProducts.add(sellerProducts4);
        sellerProducts.add(sellerProducts5);

        SellerProductsAdapter adapter = new SellerProductsAdapter(sellerProducts);
        recyclerViewSlabs.setAdapter(adapter);
        recyclerViewSlabs.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewSlabs.setNestedScrollingEnabled(false);

    }

    public void setRecyclerViewRatings(){
        ArrayList<Ratings> ratings = new ArrayList<>();

        Ratings ratings1 = new Ratings("Abraham Lincoln", 3, "12/12/2012");
        Ratings ratings2 = new Ratings("Malcolm Merlin", 4, "09/09/2017");
        Ratings ratings3 = new Ratings("Park Jimin", 2, "11/04/2019");
        Ratings ratings4 = new Ratings("Razor Kin", 4, "02/10/2019");

        ratings.add(ratings1);
        ratings.add(ratings2);
        ratings.add(ratings3);
        ratings.add(ratings4);

        RatingsAdapter ratingsAdapter = new RatingsAdapter(this, ratings);
        recyclerViewRatings.setAdapter(ratingsAdapter);
        recyclerViewRatings.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewRatings.setNestedScrollingEnabled(false);

    }

}
