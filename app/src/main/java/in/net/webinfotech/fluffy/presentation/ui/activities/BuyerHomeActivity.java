package in.net.webinfotech.fluffy.presentation.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TabHost;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.fluffy.R;
import in.net.webinfotech.fluffy.domain.model.Testing.BuyerAddress;
import in.net.webinfotech.fluffy.domain.model.Testing.SellerList;
import in.net.webinfotech.fluffy.presentation.ui.adapters.SellerListAdapter;
import in.net.webinfotech.fluffy.presentation.ui.dialogs.AddressSelectDialog;
import in.net.webinfotech.fluffy.util.BottomNavigationHelper;

public class BuyerHomeActivity extends AppCompatActivity {

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.auto_edit_text_search)
    AutoCompleteTextView autoCompleteTextView;
    private static final String[] CHICKENS = new String[] {
            "Live Chicken[1.1 - 1.3]", "Live Chicken[1.3 - 1.6]", "Live Chicken[1.6 - 1.8]", "Live Chicken[1.8 - 2.2]", "Live Chicken[22 and above]",
    };
    ArrayList<BuyerAddress> buyerAddresses;
    AddressSelectDialog addressSelectDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buyer_home);
        ButterKnife.bind(this);
        setBottomNavigationView();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, CHICKENS);
        autoCompleteTextView.setAdapter(adapter);
        initialiseDialog();
        setUpBuyerAddress();
    }

    public void initialiseDialog(){
        addressSelectDialog = new AddressSelectDialog(this, this);
    }

//    public void setUpSellerRecyclerView(){
//        ArrayList<SellerList> sellerListArrayList = new ArrayList<>();
//
//        SellerList seller1 = new SellerList("Abraham Lincoln", 3, "Sixmile", 12);
//        SellerList seller2 = new SellerList("Peter Parker", 2, "Sixmile", 15);
//        SellerList seller3 = new SellerList("Ramsey Bolton", 4, "Sixmile", 20);
//        SellerList seller4 = new SellerList("Samwell Jackson", 2, "Sixmile", 25);
//        SellerList seller5 = new SellerList("Marcos Lang", 4, "Sixmile", 31);
//
//        sellerListArrayList.add(seller1);
//        sellerListArrayList.add(seller2);
//        sellerListArrayList.add(seller3);
//        sellerListArrayList.add(seller4);
//        sellerListArrayList.add(seller5);
//
//        SellerListAdapter sellerListAdapter = new SellerListAdapter(this, sellerListArrayList);
//
//
//    }

    public void setBottomNavigationView(){
        BottomNavigationHelper.disableShiftMode(bottomNavigationView);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_home:
//                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
//                        startActivity(intent);
                        break;
                    case R.id.action_search:
                        Intent searchIntent = new Intent(getApplicationContext(), SearchActivity.class);
                        startActivity(searchIntent);
                        break;
                }
                return false;
            }
        });
    }

    @OnClick(R.id.btn_search) void onSearchClicked(){
        Intent searchIntent = new Intent(this, SearchActivity.class);
        startActivity(searchIntent);
    }

    public void setUpBuyerAddress(){
        buyerAddresses = new ArrayList<>();

        BuyerAddress buyerAddress1 = new BuyerAddress("Near Over Bridge", "Six Mile", "Guwahati", 7811818,"Assam",true);
        BuyerAddress buyerAddress2 = new BuyerAddress("Near SBI ATM", "Downtown", "Guwahati", 7811808,"Assam",false);
        BuyerAddress buyerAddress3 = new BuyerAddress("Near Market", "Super Market", "Guwahati", 711808,"Assam",false);

        buyerAddresses.add(buyerAddress1);
        buyerAddresses.add(buyerAddress2);
        buyerAddresses.add(buyerAddress3);

    }

    @OnClick(R.id.floating_btn_address) void onFloatingBtnClicked(){
        addressSelectDialog.showDialog(buyerAddresses);
    }

}
