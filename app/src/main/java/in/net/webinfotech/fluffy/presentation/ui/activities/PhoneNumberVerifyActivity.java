package in.net.webinfotech.fluffy.presentation.ui.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.fluffy.R;

public class PhoneNumberVerifyActivity extends AppCompatActivity {

    String verificationId;
    FirebaseAuth mAuth;
    @BindView(R.id.edit_text_otp_1)
    EditText editTextOTP1;
    @BindView(R.id.edit_text_otp_2)
    EditText editTextOTP2;
    @BindView(R.id.edit_text_otp_3)
    EditText editTextOTP3;
    @BindView(R.id.edit_text_otp_4)
    EditText editTextOTP4;
    @BindView(R.id.edit_text_otp_5)
    EditText editTextOTP5;
    @BindView(R.id.edit_text_otp_6)
    EditText editTextOTP6;
    @BindView(R.id.progressbar)
    ProgressBar progressBar;
    String phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_number_verify);
        ButterKnife.bind(this);
        phoneNumber = getIntent().getStringExtra("phoneNumber");
        Log.e("LogMsg", "phonenumber: " + phoneNumber);
        mAuth = FirebaseAuth.getInstance();
//        sendVerificationCode(phoneNumber);
    }

    public void listenUserSignOut(){
        FirebaseAuth.AuthStateListener authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if(firebaseAuth.getCurrentUser() == null){
                    Log.e("LogMsg", "sign out successfull");
                    sendVerificationCode(phoneNumber);
                }else {
                    Log.e("LogMsg", "Display name" +firebaseAuth.getCurrentUser().getDisplayName());
                }
            }
        };
        mAuth.addAuthStateListener(authStateListener);
    }

    private void sendVerificationCode(String number){
        progressBar.setVisibility(View.VISIBLE);
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallback
                );
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            verificationId = s;

            progressBar.setVisibility(View.GONE);
            Log.e("LogMsg", "code sent successfull " + verificationId);
        }

        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            Log.e("LogMsg", "code recveive successfll");
            progressBar.setVisibility(View.GONE);
            if(code != null){
                setOTPeditTexts(code);
                verifiyCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(PhoneNumberVerifyActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    };

    private void verifiyCode(String code){
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithCredential(credential);
    }

    private void signInWithCredential(PhoneAuthCredential credential){
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(PhoneNumberVerifyActivity.this, "Verification successfull", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(PhoneNumberVerifyActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public void setOTPeditTexts(String s){
        String OTParray[] = s.split("");
        Log.e("LogMsg", "Length" + OTParray.length);
        editTextOTP1.setText(OTParray[1]);
        editTextOTP2.setText(OTParray[2]);
        editTextOTP3.setText(OTParray[3]);
        editTextOTP4.setText(OTParray[4]);
        editTextOTP5.setText(OTParray[5]);
        editTextOTP6.setText(OTParray[6]);
    }

    public boolean checkEmptyEditText(){
        if(!editTextOTP1.getText().toString().trim().isEmpty() &&
                !editTextOTP2.getText().toString().trim().isEmpty() &&
                !editTextOTP3.getText().toString().trim().isEmpty() &&
                !editTextOTP4.getText().toString().trim().isEmpty() &&
                !editTextOTP5.getText().toString().trim().isEmpty() &&
                !editTextOTP6.getText().toString().trim().isEmpty()){
            return true;

        }else{
            return false;
        }
    }

    public String getOTPcode(){
        return editTextOTP1.getText().toString() + editTextOTP2.getText().toString() + editTextOTP3.getText().toString() + editTextOTP4.getText().toString() + editTextOTP5.getText().toString() + editTextOTP6.getText().toString();
    }

    @OnClick(R.id.btn_verify)void verifyOTPmanually(){
//        if(checkEmptyEditText()){
//            String code = getOTPcode();
//            verifiyCode(code);
//        }else{
//            Toast.makeText(this, "Some fields are missing", Toast.LENGTH_SHORT).show();
//        }

        Intent createProfileActivityIntent = new Intent(this, CreateProfileActivity.class);
        startActivity(createProfileActivityIntent);

    }

}
