package in.net.webinfotech.fluffy.domain.model.Testing;

/**
 * Created by Raj on 30-05-2019.
 */

public class ProductList {

    public String productName;
    public String sellerName;
    public double price;
    public float rating;
    public int ratingCount;

    public ProductList(String productName, String sellerName, double price, float rating, int ratingCount) {
        this.productName = productName;
        this.sellerName = sellerName;
        this.price = price;
        this.rating = rating;
        this.ratingCount = ratingCount;
    }
}
