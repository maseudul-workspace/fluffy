package in.net.webinfotech.fluffy.presentation.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.fluffy.R;
import in.net.webinfotech.fluffy.domain.model.Testing.SellerDeliveryLocation;

/**
 * Created by Raj on 31-05-2019.
 */

public class SellerDeliveryLocationAdapter extends RecyclerView.Adapter<SellerDeliveryLocationAdapter.ViewHolder>{

    ArrayList<SellerDeliveryLocation> sellerDeliveryLocations;

    public SellerDeliveryLocationAdapter(ArrayList<SellerDeliveryLocation> sellerDeliveryLocations) {
        this.sellerDeliveryLocations = sellerDeliveryLocations;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_seller_delivery_locations, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewLocation.setText(sellerDeliveryLocations.get(position).location);
    }

    @Override
    public int getItemCount() {
        return sellerDeliveryLocations.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_location)
        TextView txtViewLocation;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
