package in.net.webinfotech.fluffy.domain.model.Testing;

/**
 * Created by Raj on 28-05-2019.
 */

public class SellerList {

    public String name;
    public int rating;
    public String location;
    public int ratingCount;

    public SellerList(String name, int rating, String location, int ratingCount) {
        this.name = name;
        this.rating = rating;
        this.location = location;
        this.ratingCount = ratingCount;
    }
}
