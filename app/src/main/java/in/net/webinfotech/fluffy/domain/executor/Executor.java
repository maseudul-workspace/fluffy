package in.net.webinfotech.fluffy.domain.executor;

import in.net.webinfotech.fluffy.domain.interactor.base.AbstractInteractor;

/**
 * Created by Raj on 20-05-2019.
 */

public interface Executor {
    /**
     * This method should call the interactor's run method and thus start the interactor. This should be called
     * on a background thread as interactors might do lengthy operations.
     *
     * @param interactor The interactor to run.
     */
    void execute(final AbstractInteractor interactor);
}
