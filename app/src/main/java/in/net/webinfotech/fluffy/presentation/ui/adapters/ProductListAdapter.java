package in.net.webinfotech.fluffy.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.fluffy.R;
import in.net.webinfotech.fluffy.domain.model.Testing.ProductList;

/**
 * Created by Raj on 30-05-2019.
 */

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder>{

    public interface Callback{
        void onAddToCartClicked();
    }

    Context mContext;
    ArrayList<ProductList> productLists;
    Callback mCallback;

    public ProductListAdapter(Context mContext, ArrayList<ProductList> productLists, Callback callback) {
        this.mContext = mContext;
        this.productLists = productLists;
        this.mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_product_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewSellerName.setText(productLists.get(position).sellerName);
        holder.getTxtViewProductName.setText(productLists.get(position).productName);
        holder.txtViewProductPrice.setText("Rs. " + productLists.get(position).price);
        holder.ratingBar.setRating(productLists.get(position).rating);
        holder.txtViewRatingCount.setText(Integer.toString(productLists.get(position).ratingCount));
        holder.imgViewAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onAddToCartClicked();
            }
        });
    }

    @Override
    public int getItemCount() {
        return productLists.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_seller_name)
        TextView txtViewSellerName;
        @BindView(R.id.txt_view_product_name)
        TextView getTxtViewProductName;
        @BindView(R.id.rating_bar_seller)
        RatingBar ratingBar;
        @BindView(R.id.txt_view_rating_counts)
        TextView txtViewRatingCount;
        @BindView(R.id.txt_view_product_price)
        TextView txtViewProductPrice;
        @BindView(R.id.img_view_add_to_cart)
        ImageView imgViewAddToCart;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
