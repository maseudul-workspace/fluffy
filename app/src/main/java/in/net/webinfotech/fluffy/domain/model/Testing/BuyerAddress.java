package in.net.webinfotech.fluffy.domain.model.Testing;

/**
 * Created by Raj on 15-06-2019.
 */

public class BuyerAddress {
    public String landmark;
    public String area;
    public String city;
    public long pin;
    public String state;
    public boolean isSelected;

    public BuyerAddress(String landmark, String area, String city, long pin, String state, boolean isSelected) {
        this.landmark = landmark;
        this.area = area;
        this.city = city;
        this.pin = pin;
        this.state = state;
        this.isSelected = isSelected;
    }

}
