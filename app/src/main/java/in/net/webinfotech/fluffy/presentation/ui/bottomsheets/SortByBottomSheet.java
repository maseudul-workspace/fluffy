package in.net.webinfotech.fluffy.presentation.ui.bottomsheets;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import in.net.webinfotech.fluffy.R;

/**
 * Created by Raj on 29-05-2019.
 */

public class SortByBottomSheet extends BottomSheetDialogFragment {

    public interface Callback{
        void onSortBySelected(String msg);
    }
    Callback mCallback;
    String selectedSortBy;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle mArgs = getArguments();
        selectedSortBy = mArgs.getString("sortBy");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottom_sheet_sort_by, container, false);
        RadioGroup sortByRadioGroup = view.findViewById(R.id.radio_group_sort_by);
        RadioButton priceLowRadioBtn = view.findViewById(R.id.radio_btn_price_low);
        RadioButton priceHighRadioBtn = view.findViewById(R.id.radio_btn_price_high);
        switch (selectedSortBy){
            case "Price: Low To High":
                priceLowRadioBtn.setChecked(true);
                break;
            case "Price: High To Low":
                priceHighRadioBtn.setChecked(true);
                break;

        }
        sortByRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton checkedRadioBtn = (RadioButton)radioGroup.findViewById(i);
                mCallback.onSortBySelected(checkedRadioBtn.getText().toString());
                dismiss();
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (Callback) context;
        } catch (ClassCastException e){
            throw new ClassCastException(context.toString() + "must implement methods");
        }
    }

}
