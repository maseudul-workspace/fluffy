package in.net.webinfotech.fluffy.domain.model.Testing;

/**
 * Created by Raj on 31-05-2019.
 */

public class Ratings {
    public String user;
    public float rating;
    public String date;

    public Ratings(String user, float rating, String date) {
        this.user = user;
        this.rating = rating;
        this.date = date;
    }
}
