package in.net.webinfotech.fluffy.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.View;

import in.net.webinfotech.fluffy.R;

/**
 * Created by Raj on 20-06-2019.
 */

public class OrderRejectDialog {
    AlertDialog.Builder builder;
    Context mContext;
    Activity mActivity;
    AlertDialog dialog;
    View dialogContainer;

    public OrderRejectDialog(Context mContext, Activity mActivity) {
        this.mContext = mContext;
        this.mActivity = mActivity;
    }

    public void showDialog(){
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.dialog_order_reject_layout, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.show();
    }

}
