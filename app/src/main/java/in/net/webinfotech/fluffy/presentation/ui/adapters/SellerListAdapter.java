package in.net.webinfotech.fluffy.presentation.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.fluffy.R;
import in.net.webinfotech.fluffy.domain.model.Testing.SellerList;
import in.net.webinfotech.fluffy.presentation.ui.activities.SellerDetailsActivity;
import in.net.webinfotech.fluffy.util.GlideHelper;

/**
 * Created by Raj on 28-05-2019.
 */

public class SellerListAdapter extends RecyclerView.Adapter<SellerListAdapter.ViewHolder> {

    Context mContext;
    ArrayList<SellerList> sellerLists;

    public SellerListAdapter(Context mContext, ArrayList<SellerList> sellerLists) {
        this.mContext = mContext;
        this.sellerLists = sellerLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_seller_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewSellerName.setText(this.sellerLists.get(position).name);
        holder.txtViewSellerLocation.setText(this.sellerLists.get(position).location);
        holder.ratingBar.setRating(sellerLists.get(position).rating);
        holder.txtViewRatingCount.setText(Integer.toString(sellerLists.get(position).ratingCount));
        String nameInitial = getInitial(sellerLists.get(position).name);
        Log.e("LogMsg", "klll" + nameInitial);
        switch (nameInitial){
            case "A":
                GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewSeller, R.drawable.a);
                break;
            case "M":
                GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewSeller, R.drawable.m);
                break;
            case "P":
                GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewSeller, R.drawable.p);
                break;
            case "R":
                GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewSeller, R.drawable.r);
                break;
            case "S":
                GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewSeller, R.drawable.s);
                break;
        }
        holder.btnSellerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sellerDetailIntent = new Intent(mContext, SellerDetailsActivity.class);
                mContext.startActivity(sellerDetailIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return sellerLists.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_seller_name)
        TextView txtViewSellerName;
        @BindView(R.id.txt_view_seller_location)
        TextView txtViewSellerLocation;
        @BindView(R.id.rating_bar_seller)
        RatingBar ratingBar;
        @BindView(R.id.txt_view_rating_counts)
        TextView txtViewRatingCount;
        @BindView(R.id.img_view_seller)
        ImageView imgViewSeller;
        @BindView(R.id.btn_seller_view)
        Button btnSellerView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public String getInitial(String s){
        String[] stringArr = s.split("");
        return stringArr[1];
    }

}
