package in.net.webinfotech.fluffy.presentation.ui.bottomsheets;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import in.net.webinfotech.fluffy.R;

/**
 * Created by Raj on 29-05-2019.
 */

public class SlabsBottomSheet extends BottomSheetDialogFragment {

    public interface Callback{
        void onSlabSelected(String msg);
    }
    Callback mCallback;
    String selectedSlab;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle mArgs = getArguments();
        selectedSlab = mArgs.getString("selectedSlab");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottom_sheet_slabs, container, false);
        RadioGroup slabRadioGroup = view.findViewById(R.id.radio_group_slabs);
        RadioButton slab_radio_btn_11_13 = view.findViewById(R.id.radio_btn_slab_11_13);
        RadioButton slab_radio_btn_13_16 = view.findViewById(R.id.radio_btn_slab_13_16);
        RadioButton slab_radio_btn_16_18 = view.findViewById(R.id.radio_btn_slab_16_18);
        RadioButton slab_radio_btn_18_22 = view.findViewById(R.id.radio_btn_slab_18_22);
        RadioButton slab_radio_btn_22 = view.findViewById(R.id.radio_btn_slab_22);

        switch (selectedSlab){
            case "Live Chicken[1.1 - 1.3]":
                slab_radio_btn_11_13.setChecked(true);
                break;
            case "Live Chicken[1.3 - 1.6]":
                slab_radio_btn_13_16.setChecked(true);
                break;
            case "Live Chicken[1.6 - 1.8]":
                slab_radio_btn_16_18.setChecked(true);
                break;
            case "Live Chicken[1.8 - 2.2]":
                slab_radio_btn_18_22.setChecked(true);
                break;
            case "Live Chicken[22 and above]":
                slab_radio_btn_22.setChecked(true);
                break;

        }
        slabRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton checkedRadioBtn = (RadioButton)radioGroup.findViewById(i);
                mCallback.onSlabSelected(checkedRadioBtn.getText().toString());
                dismiss();
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (Callback) context;
        } catch (ClassCastException e){
            throw new ClassCastException(context.toString() + "must implement methods");
        }
    }

}
