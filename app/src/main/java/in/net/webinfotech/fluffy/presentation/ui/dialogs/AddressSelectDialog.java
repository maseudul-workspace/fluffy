package in.net.webinfotech.fluffy.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

import in.net.webinfotech.fluffy.R;
import in.net.webinfotech.fluffy.domain.model.Testing.BuyerAddress;
import in.net.webinfotech.fluffy.presentation.ui.adapters.BuyerAddressListAdapter;

/**
 * Created by Raj on 17-06-2019.
 */

public class AddressSelectDialog implements BuyerAddressListAdapter.Callback{

    AlertDialog.Builder builder;
    Context mContext;
    Activity mActivity;
    AlertDialog dialog;
    View dialogContainer;
    ArrayList<BuyerAddress> buyerAddresses;
    RecyclerView recyclerView;
    BuyerAddressListAdapter adapter;

    public AddressSelectDialog(Context mContext, Activity mActivity) {
        this.mContext = mContext;
        this.mActivity = mActivity;
    }

    public void showDialog(ArrayList<BuyerAddress> addresses){
        this.buyerAddresses = addresses;
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.dialog_address_select_layout, null);
        recyclerView = (RecyclerView) dialogContainer.findViewById(R.id.recycler_view_buyer_address);
        ImageView imgViewCancel = (ImageView) dialogContainer.findViewById(R.id.img_view_cancel);
        imgViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        adapter = new BuyerAddressListAdapter(mContext, buyerAddresses, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.show();
    }

    @Override
    public void onLayoutClicked(final int position) {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                for(int i = 0; i < buyerAddresses.size(); i++){
                    buyerAddresses.get(i).isSelected = false;
                    }
                    buyerAddresses.get(position).isSelected = true;
                    adapter.updateData(buyerAddresses);
                }
        }, 300);
    }
}
