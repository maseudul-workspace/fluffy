package in.net.webinfotech.fluffy.presentation.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.fluffy.R;
import in.net.webinfotech.fluffy.domain.model.Testing.SellerProductList;

/**
 * Created by Raj on 03-06-2019.
 */

public class SellerProductListAdapter extends RecyclerView.Adapter<SellerProductListAdapter.ViewHolder>{

    public interface Callback{
        void onEditClicked();
    }

    Callback mCallback;
    ArrayList<SellerProductList> sellerProductLists;

    public SellerProductListAdapter(ArrayList<SellerProductList> sellerProductLists, Callback callback) {
        this.sellerProductLists = sellerProductLists;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_seller_products_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewProductName.setText(sellerProductLists.get(position).productName);
        holder.txtViewPrice.setText("Rs. " + sellerProductLists.get(position).price + "/kg");
        holder.txtViewQty.setText(Integer.toString(sellerProductLists.get(position).quantity));
        holder.txtViewQtyEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onEditClicked();
            }
        });
    }

    @Override
    public int getItemCount() {
        return sellerProductLists.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;
        @BindView(R.id.txt_view_qty)
        TextView txtViewQty;
        @BindView(R.id.txt_view_qty_edit)
        TextView txtViewQtyEdit;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
