package in.net.webinfotech.fluffy.presentation.ui.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationMenu;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.fluffy.R;
import in.net.webinfotech.fluffy.domain.model.Testing.ProductList;
import in.net.webinfotech.fluffy.presentation.ui.adapters.ProductListAdapter;
import in.net.webinfotech.fluffy.presentation.ui.bottomsheets.SlabsBottomSheet;
import in.net.webinfotech.fluffy.presentation.ui.bottomsheets.SortByBottomSheet;
import in.net.webinfotech.fluffy.presentation.ui.dialogs.AddToCartDialog;
import in.net.webinfotech.fluffy.presentation.ui.dialogs.AddressSelectDialog;
import in.net.webinfotech.fluffy.util.BottomNavigationHelper;

public class SearchActivity extends AppCompatActivity implements SortByBottomSheet.Callback, SlabsBottomSheet.Callback, ProductListAdapter.Callback{

    SearchView searchView;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.txt_view_sort_by)
    TextView txtViewSortBy;
    @BindView(R.id.txt_view_slabs)
    TextView txtViewSlabs;
    @BindView(R.id.recycler_view_product_list)
    RecyclerView recycler_view_product_list;
    SortByBottomSheet sortByBottomSheet;
    SlabsBottomSheet slabsBottomSheet;
    String sortBy = "Price: Low To High";
    String selectedSlab = "Live Chicken[1.1 - 1.3]";
    AddToCartDialog addToCartDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        setBottomNavigationView();
        setProductListAdapter();
        initialiseAddToCartDialog();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.search_menu, menu);
//        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
//        searchView = (android.support.v7.widget.SearchView) myActionMenuItem.getActionView();
//        myActionMenuItem.expandActionView();
//        return true;
//    }

    public void initialiseAddToCartDialog(){
        addToCartDialog = new AddToCartDialog(this, this);
    }

    public void setBottomNavigationView(){
        BottomNavigationHelper.disableShiftMode(bottomNavigationView);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(1);
        menuItem.setChecked(true);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_home:
                        Intent buyerHomeIntent = new Intent(getApplicationContext(), BuyerHomeActivity.class);
                        startActivity(buyerHomeIntent);
                        break;
                    case R.id.action_search:
//                        Intent searchIntent = new Intent(getApplicationContext(), SearchActivity.class);
//                        startActivity(searchIntent);
                        break;
                }
                return false;
            }
        });
    }

    public void setProductListAdapter(){
        ArrayList<ProductList> productLists = new ArrayList<>();

        ProductList product1 = new ProductList("Live Chicken[1.1 - 1.3]", "Bruce Wayne", 122, 4, 23);
        ProductList product2 = new ProductList("Live Chicken[1.1 - 1.3]", "Peter Parker", 120, 5, 16);
        ProductList product3 = new ProductList("Live Chicken[1.1 - 1.3]", "Tony Stark", 121, 4, 33);
        ProductList product4 = new ProductList("Live Chicken[1.1 - 1.3]", "Bryan Adams", 1222, 4, 23);

        productLists.add(product1);
        productLists.add(product2);
        productLists.add(product3);
        productLists.add(product4);

        ProductListAdapter adapter = new ProductListAdapter(this, productLists, this);
        recycler_view_product_list.setLayoutManager(new LinearLayoutManager(this));
        recycler_view_product_list.setAdapter(adapter);

    }

    @OnClick(R.id.relative_layout_sort_by) void onSortByClicked(){
        Bundle args = new Bundle();
        args.putString("sortBy", sortBy);
        sortByBottomSheet = new SortByBottomSheet();
        sortByBottomSheet.setArguments(args);
        sortByBottomSheet.show(getSupportFragmentManager(), "Sort By");
    }

    @OnClick(R.id.relative_layout_slabs) void onSlabsClicked(){
        Bundle args = new Bundle();
        args.putString("selectedSlab", selectedSlab);
        slabsBottomSheet = new SlabsBottomSheet();
        slabsBottomSheet.setArguments(args);
        slabsBottomSheet.show(getSupportFragmentManager(), "Choose Slab");
    }

    @Override
    public void onSortBySelected(String msg) {
        sortBy = msg;
        txtViewSortBy.setText(msg);
    }

    @Override
    public void onSlabSelected(String msg) {
        selectedSlab = msg;
        txtViewSlabs.setText(msg);
    }

    @Override
    public void onAddToCartClicked() {
        addToCartDialog.showDialog();
    }
}
