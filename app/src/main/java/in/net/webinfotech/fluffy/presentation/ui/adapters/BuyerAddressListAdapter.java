package in.net.webinfotech.fluffy.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.fluffy.R;
import in.net.webinfotech.fluffy.domain.model.Testing.BuyerAddress;

/**
 * Created by Raj on 15-06-2019.
 */

public class BuyerAddressListAdapter extends RecyclerView.Adapter<BuyerAddressListAdapter.ViewHolder>{

    public interface Callback{
        void onLayoutClicked(int position);
    }

    Context mContext;
    ArrayList<BuyerAddress> buyerAddresses;
    Callback mCallback;

    public BuyerAddressListAdapter(Context mContext, ArrayList<BuyerAddress> buyerAddresses, Callback callback) {
        this.mContext = mContext;
        this.buyerAddresses = buyerAddresses;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_address_list_dialog, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.txtViewArea.setText(buyerAddresses.get(position).area);
        String addressMain = buyerAddresses.get(position).landmark + ", " + buyerAddresses.get(position).city + ", " + buyerAddresses.get(position).state + ", " + buyerAddresses.get(position).pin;
        holder.txtViewAddressMain.setText(addressMain);
        if(buyerAddresses.get(position).isSelected){
            holder.txtViewEditText.setVisibility(View.VISIBLE);
            holder.imgViewSelected.setVisibility(View.VISIBLE);
            holder.layoutMain.setBackground(mContext.getResources().getDrawable(R.drawable.ripple_card_light_orange));
        }else{
            holder.txtViewEditText.setVisibility(View.GONE);
            holder.imgViewSelected.setVisibility(View.GONE);
            holder.layoutMain.setBackground(mContext.getResources().getDrawable(R.drawable.ripple_card_view));
        }

        holder.layoutMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onLayoutClicked(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return buyerAddresses.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_address_main)
        TextView txtViewAddressMain;
        @BindView(R.id.txt_view_area)
        TextView txtViewArea;
        @BindView(R.id.txt_view_edit_text)
        TextView txtViewEditText;
        @BindView(R.id.img_view_selected)
        ImageView imgViewSelected;
        @BindView(R.id.layout_main)
        View layoutMain;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateData(ArrayList<BuyerAddress> buyerAddresses){
        this.buyerAddresses = buyerAddresses;
        notifyDataSetChanged();
    }

}
