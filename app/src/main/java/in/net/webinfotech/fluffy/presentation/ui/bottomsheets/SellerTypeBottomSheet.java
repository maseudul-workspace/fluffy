package in.net.webinfotech.fluffy.presentation.ui.bottomsheets;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import in.net.webinfotech.fluffy.R;

/**
 * Created by Raj on 27-05-2019.
 */

public class SellerTypeBottomSheet extends BottomSheetDialogFragment {

    public interface Callback{
        void onRadioBtnSelected(String msg);
    }
    Callback mCallback;
    String selectedText;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle mArgs = getArguments();
        selectedText = mArgs.getString("sellerType");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottom_sheet_buyer_type_layout, container, false);
        RadioGroup sortByRadioGroup = view.findViewById(R.id.radio_group_seller_type);
        RadioButton shopkeeperRadioBtn = view.findViewById(R.id.radio_btn_shopkeeper);
        RadioButton hotelierRadioBtn = view.findViewById(R.id.radio_btn_hotelier);
        RadioButton catererRadioBtn = view.findViewById(R.id.radio_btn_caterer);
        switch (selectedText){
            case "Shopkeeper":
                shopkeeperRadioBtn.setChecked(true);
                break;
            case "Hotelier":
                hotelierRadioBtn.setChecked(true);
                break;
            case "Caterer":
                catererRadioBtn.setChecked(true);
                break;
        }
        sortByRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton checkedRadioBtn = (RadioButton)radioGroup.findViewById(i);
                mCallback.onRadioBtnSelected(checkedRadioBtn.getText().toString());
            }
        });

        getDialog().setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                BottomSheetDialog d = (BottomSheetDialog) dialogInterface;
                View bottomSheetInternal = d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheetInternal).setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (Callback) context;
        } catch (ClassCastException e){
            throw new ClassCastException(context.toString() + "must implement methods");
        }
    }

}
